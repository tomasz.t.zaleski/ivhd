#ifdef USE_CUDA

#include "graph/generate/Faiss.h"

#include <cstdio>
#include <cassert>

#include <faiss/gpu/GpuIndexFlat.h>
#include <faiss/gpu/GpuIndexIVFFlat.h>
#include <faiss/gpu/StandardGpuResources.h>

namespace viskit::graph::generate
{
    Faiss::Faiss(core::System &system)
            : GraphGenerator(system)
    {

    }

    void Faiss::generate(particles::ParticleSystem &ps, graph::Graph &graph, size_t k,
                                         bool distancesEqualOne)
    {
        graph.initialize(ps.countParticles());

        const auto d = ps.datasetInfo().dimensionality;
        const auto count = ps.datasetInfo().count;

        const auto nb = count;  // size of database

        std::vector<float> xb;

        const auto coordinates = ps.originalCoordinates();

        for (auto i = 0; i < nb; i++) {
            for (auto j = 0; j < d; j++) {
                xb.emplace_back(coordinates[i].first[j]);
            }
        }

        m_ext_system.logger().logInfo("[FAISS kNN Generator] Searching for nearest neighbors...");

        faiss::gpu::StandardGpuResources res;

        faiss::gpu::GpuIndexFlatL2 index_flat(&res, d);
        // faiss::gpu::GpuIndexFlatIP index_flat(&res, d);
        // index_flat.train(nb, xb.data());
        index_flat.add(nb, xb.data());  // add vectors to the index

        k += 1;
        {
            auto *I = new long[k * count];
            auto *D = new float[k * count];

            index_flat.search(nb, xb.data(), k, D, I);

            for (int i = 0; i < count; i++)
            {
                for (int j = 1; j < k; j++)
                {
                    if (!distancesEqualOne) {
                        graph.addNeighbors(Neighbors(I[i * k], I[i * k + j], D[i * k + j], NeighborsType::Near));
                    } else {
                        graph.addNeighbors(Neighbors(I[i * k], I[i * k + j], 1.0f, NeighborsType::Near));
                    }
                }
                printf("\n");
            }

            delete[] I;
            delete[] D;
        }

        graph.neighborsCounter.nearestNeighbors = k-1;
    }

    void Faiss::generate(std::vector<std::pair<viskit::DataPoint, particles::DataPointLabel>> points, Graph &graph, size_t k,
                         bool distancesEqualOne)
    {
        graph.initialize(points.size());

        const auto d = points[0].first.size();
        const auto count = points.size();

        const auto nb = count;  // size of database

        std::vector<float> xb;

        for (auto i = 0; i < nb; i++) {
            for (auto j = 0; j < d; j++) {
                xb.push_back(points[i].first[j]);
            }
        }

        m_ext_system.logger().logInfo("[FAISS kNN Generator] Searching for nearest neighbors...");

        faiss::gpu::StandardGpuResources res;

        faiss::gpu::GpuIndexFlatL2 index_flat(&res, d);
        //faiss::gpu::GpuIndexIVFFlat index_ivf(&res, d, static_cast<int>(sqrt(count)), faiss::METRIC_L2);

        index_flat.train(nb, xb.data());
        index_flat.add(nb, xb.data());  // add vectors to the index


        {
            auto *I = new long[k * count];
            auto *D = new float[k * count];

            index_flat.search(nb, xb.data(), k, D, I);

            for (int i = 0; i < count; i++) {
                for (int j = 0; j < k; j++) {
                    const auto index = I[i * k + j];
                    if (i != index) {
                        if (!distancesEqualOne) {
                            graph.addNeighbors(Neighbors(i, index, D[i * k + j], NeighborsType::Near));
                        } else {
                            graph.addNeighbors(Neighbors(i, index, 1.0f, NeighborsType::Near));
                        }
                    }
                }
            }

            delete[] I;
            delete[] D;
        }

        graph.neighborsCounter.nearestNeighbors = k;
    }
}

#endif